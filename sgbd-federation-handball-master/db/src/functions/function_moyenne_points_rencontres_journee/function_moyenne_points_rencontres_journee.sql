-- ============================================
-- file: function_moyenne_points_rencontres_journee.sql
-- base: federation
-- date: Nov 2020
-- role: Afficher la moyenne des points marques
--       lors des rencontres a une date donnee
-- ============================================


\i src/functions/function_scores_matchs_journee/function_scores_matchs_journee.sql

CREATE OR REPLACE FUNCTION moyenne_points_rencontres_journee(journee DATE)
RETURNS TABLE (moyenne_pts INTEGER)
LANGUAGE SQL
AS $$
    SELECT AVG(total_points)
    FROM (
            SELECT (total_points_acc + total_points_ext) AS total_points
            FROM scores_matchs_journee(journee)
             ) AS total_pts_rencontre;
$$;
