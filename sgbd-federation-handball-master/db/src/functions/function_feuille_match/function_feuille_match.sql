-- ============================================
-- file: function_feuille_match.sql
-- base: federation
-- date: Nov 2020
-- role: Consulter les informations par joueur pour un match donne
-- ============================================

\i src/views/view_score_joueurs_rencontres/view_score_joueurs_rencontres.sql

CREATE OR REPLACE FUNCTION feuille_match (id_rencontre_recherche integer)
RETURNS TABLE (id_equipe integer, nom_equipe varchar, numero_licence_joueur numeric(12), prenom_joueur varchar,
    nom_joueur varchar, points_marques integer, nombre_fautes integer)
LANGUAGE SQL
AS $$
SELECT e.id_equipe, nom_equipe, numero_licence, prenom, nom, points_marques, nombre_fautes
FROM score_joueurs_rencontres sjr
     INNER JOIN equipes e ON sjr.id_equipe = e.id_equipe
WHERE id_rencontre = id_rencontre_recherche;
$$;


