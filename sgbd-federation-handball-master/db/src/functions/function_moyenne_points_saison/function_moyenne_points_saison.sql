-- ============================================
-- file: function_moyenne_points_saison.sql
-- base: federation
-- date: Nov 2020
-- role: Afficher la moyenne des points marques
--       lors d'une saison
-- ============================================


\i src/views/view_scores_equipes_rencontres/view_scores_equipes_rencontres.sql

CREATE OR REPLACE FUNCTION moyenne_points_saison(id_saison_recherchee INTEGER)
RETURNS TABLE (moyenne_pts INTEGER)
LANGUAGE SQL
AS $$
    SELECT AVG(total_points)
    FROM (
            SELECT (total_points_acc + total_points_ext) AS total_points
            FROM scores_equipes_rencontres ser
                INNER JOIN rencontres r ON r.id_rencontre = ser.id_rencontre
                INNER JOIN saisons s ON s.id_saison = r.id_saison
            WHERE r.id_saison = id_saison_recherchee
             ) AS total_pts_rencontre;
$$;
