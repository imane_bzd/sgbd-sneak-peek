-- ============================================
-- file: function_classement_joueurs_journee.sql
-- base: federation
-- date: Nov 2020
-- role: Consulter le classement des n meilleurs joueurs
--       d'une categorie pour une journee
-- ============================================

\i src/views/view_score_joueurs_rencontres/view_score_joueurs_rencontres.sql

CREATE OR REPLACE FUNCTION classement_joueurs_journee (nb_joueurs integer,
                                                       categorie integer, journee date)
    RETURNS TABLE (numero_licence_joueur numeric(12), prenom_joueur varchar,
                   nom_joueur varchar, points_marques_journee integer)
    LANGUAGE SQL
AS $$
SELECT numero_licence, prenom, nom, SUM(points_marques)
FROM score_joueurs_rencontres sjr
        INNER JOIN rencontres r ON sjr.id_rencontre = r.id_rencontre
        INNER JOIN equipes e ON sjr.id_equipe = e.id_equipe
        INNER JOIN categories_niveaux cn ON cn.id_categorie_niveau = e.id_categorie_niveau
        INNER JOIN categories c ON cn.id_categorie = c.id_categorie
WHERE r.date_rencontre = journee
AND c.id_categorie = categorie
GROUP BY numero_licence, prenom, nom
ORDER BY SUM(points_marques) DESC
LIMIT nb_joueurs;
$$;


