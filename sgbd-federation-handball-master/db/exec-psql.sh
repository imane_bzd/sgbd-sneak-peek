#!/bin/bash

set -o allexport
source ./db.env
set +o allexport

if [[ -v SQLC_STDIN ]];
then
    echo $SQLC_STDIN | docker-compose exec -T db psql -U "$POSTGRES_USER" "$POSTGRES_DB"
else
    docker-compose exec db psql -U "$POSTGRES_USER" "$POSTGRES_DB"
fi

