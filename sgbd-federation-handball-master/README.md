# FFFHB — Notice d'utilisation


Le projet a consisté à traduire un besoin métier en une base de données relationnelle PostgreSQL.


## TL;DR (_Too Long; Didn’t Read_)

Différentes installations sont possibles : une version minimale, qui contient uniquement la base de données
PostgreSQL, ou une installation complète avec interface web, qui requiert Composer, Symfony, etc.

La version minimale du projet peut être utilisée avec une base PostgreSQL déjà installée, en configurant
manuellement et modifiant différents fichiers, mais l'exécution des tests _n'est pas automatisée_. 
L'installation via Docker est donc recommandée, car elle est rapide et nécessite peu de configuration.

L'installation complète est plus laborieuse, moins automatisée et nécessite d'installer et de configurer
différents outils (notamment Symfony et Composer). Même avec Docker, l'installation peut être longue :
la construction complète des images prend 10 à 20 minutes, sans compter les étapes de configuration.

Enfin, au lieu de tester toutes les requêtes sur le même jeu de données d'exemple ("fixtures"),
chaque requête est testée unitairement sur un jeu de données adéquat.
Leur fonctionnement est détaillé en section _Tests_.


## Installation & utilisation de la version minimale

Ces instructions permettent d'obtenir une copie locale de la base de données PostgreSQL (sans l'interface web).

### Pré-requis

Docker et Docker-Compose ont été utilisés afin de limiter les différences de configuration entre les machines
entre les membres du groupe. 
Même si Docker et Docker-Compose ne sont pas indispensables, ils sont fortement recommandés.
Par ailleurs, Make est utilisé afin d'exécuter les requêtes demandées et d'exécuter les tests.

### Installation avec Docker et Docker-Compose (recommandé)

Depuis la racine du dépôt, déplacez-vous dans le répertoire `db/`.

Copiez le fichier d'exemple `db.env.example` en tant que `db.env`, et configurez les variables au besoin.
Pour un développement en local, les valeurs par défaut peuvent être conservées.

```sh
cp db.env.example db.env
```

Il ne reste alors plus qu'à exécuter le script `./run-docker.sh`.
Celui-ci se charge de télécharger et construire les images, puis créer et lancer les conteneurs.
Les tables, vues, fonctions, procédures et déclencheurs sont créés automatiquement,
et le jeu de données de "fixtures" est inséré.

> Note : À chaque exécution, `./run-docker.sh` supprime et recrée les volumes, ce qui
> permet de repartir d'un environnement vierge. Alternativement, les scripts du répertoire `utils/`
> peuvent être utilisés : `drop_federation.sql` (supprime les tables, fonctions, vues...),
> `clear_federation.sql` (supprime les n-uplets), `recreate_federation.sql` (supprime et recrée les tables,
> fonctions, vues...), `insert_federation.sql` (recrée les "fixtures", à savoir les n-uplets initiaux).

Par ailleurs, le script `./exec_psql.sh` permet d'accéder à la console PSQL depuis l'intérieur du conteneur
PostgreSQL.
Les scripts SQL peuvent y être exécutés avec la commande PSQL `\i <nom-du-script.sql>`, en spécifiant un chemin
relatif au répertoire `db` (par exemple, `\i utils/recreate_federation.sql`).

Les requêtes différentes requêtes et leurs tests peuvent être exécutés depuis cette console.
Toutefois, il peut être plus commode d'utiliser le `Makefile`, dont le fonctionnement est expliqué dans
la section _Tests_.

Un conteneur `adminer` est également créé par `./run-docker.sh`. Ce logiciel, similaire à PHP My Admin,
est accessible sur le port 8080 : <http://localhost:8080>.


### Installation sans Docker

Depuis la racine du dépôt, déplacez-vous dans le répertoire `db/`.

Les scripts SQL peuvent être exécutés directement dans une console PSQL.
Le répertoire `utils/` contient divers utilitaires afin d'ajouter, supprimer, modifier des tables ou des
n-uplets. En particulier, le script `utils/create_federation.sql` permet de créer les tables et leurs contraintes,
mais il ne crée pas les vues, fonctions, déclencheurs..._
Ce choix est délibéré : ces créations sont réalisées par les différents scripts contenus dans le répertoire
`src`, afin de les tester unitairement (voir section _Tests_).

### Tests

**Attention : ** Les règles du `Makefile` sont prévues pour une utilisation avec Docker. Une adaptation
du script `exec-psql.sh` est nécessaire pour une installation de PostgreSQL en local.

Le `Makefile` permet d'exécuter les différentes requêtes demandées dans le sujet, ainsi que de les tester unitairement.
Chaque requête de création de fonction, déclencheur, vue... est enregistrée dans un sous-répertoire de `src/`.
La règle par défaut du Makefile (`make`) exécute tous les scripts SQL de ce répertoire.

Toutes les requêtes demandées ont également un jeu de tests associé dans le répertoire `tests/`.
Au lieu d'utiliser un unique jeu de données de "fixtures", chaque requête est accompagnée d'un jeu de tests spécifique.
Ainsi, pour chaque test, on supprime tous les n-uplets de la base, on insère le jeu de données de test de la requête
et on vérifie que les n-uplets _valides_ (qui devraient être renvoyés par la requête) sont présents et que les
n-uplets _invalides_ sont absents.

La règle `test` du Makefile (`make test`) exécute tous les scripts de test `.sql` et génère un fichier
`.out` dans le même répertoire que chaque test.
Il faut alors vérifier manuellement que les données renvoyées par chaque test sont cohérentes, en
comparant le domaine valide attendu et les résultats réellement obtenus.

```
src/functions
`-- function_joueurs_date
    `-- function_joueurs_date.sql

tests/functions
`-- function_joueurs_date
    |-- test_function_joueurs_date.out // généré par make test
    `-- test_function_joueurs_date.sql
```

Par exemple, l'arborescence de fichiers pour la fonction `joueurs_date()` est représentée ci-dessus.
Lors d'un `make test`, le fichier `test_function_joueurs_date.sql` est exécuté, et la sortie standard
est redirigée vers `test_function_joueurs_date.out`. Le test supprime tous les n-uplets et en insère de
nouveaux. On cherche à vérifier que l'appel de `joueurs_date(2020-09-20)` renvoie bien les joueurs
qui ont joué un match le `2020-09-20`, mais pas ceux qui ont participé à des matchs à une autre date.
En particulier, ce test insère deux joueurs : un joueur d'ID `3` jouant le `2020-09-20`, dans la section
`DOMAINE VALIDE`, et un joueur d'ID `6` à une autre date, dans la section `DOMAINE INVALIDE`.
On vérifie alors à la fin du fichier `.out` que seul le joueur d'ID `3` est renvoyé par la requête :

```sql
SELECT * FROM joueurs_date(to_date('20/09/2020', 'dd/mm/yyyy'));
```

Dans le fichier `.out`, on obtient le résultat :

```
SELECT * FROM joueurs_date(to_date('20/09/2020', 'dd/mm/yyyy'));
-[ RECORD 1 ]---+-----------
id_personne     | 3
prenom          | David
nom             | Jérôme
date_recherchee | 2020-09-20

-- [FIN_REQ]
```

La fonction `joueurs_date()` semble donc correcte pour les cas testés.



## Utilisation en ligne

L'interface web est accessible à l'adresse <https://federation.alreadyuptodate.fr/>.
La section publique du site est accessible sans authentification, et permet uniquement
de consulter les résultats des requêtes du sujet (classement, feuilles de matchs...).

Le panel de gestion, qui permet de réaliser des opérations CRUD sur les entités, nécessite néanmoins une
authentification. Deux utilisateurs de test sont proposés :

- un compte "club", ayant des droits limités de lecture et de modification limités
  (login : `handballaytre`, mot de passe : `clubPwd`)
- un compte "fédération", qui dispose de droits administrateurs sur les entités de l'application
  (login : `fffhb`, mot de passe : `federationPwd`)




## Installation & utilisation complète

Les instructions suivantes permettent d'obtenir une version locale de l'interface web.
L'installation est plus complexe que pour la version minimale.

Tout d'abord, ouvrez le fichier `web/.env` et vérifiez les valeurs des variables d'environnement de Symfony.
Pour un environnement de développement, il faut principalement s'assurer que la valeur de `DATABASE_URL`
correspond permet bien l'accès à la base de données. Pour une installation avec Docker, les valeurs
par défaut sont satisfaisantes.

### Création des images et des conteneurs avec Docker

Depuis la racine du projet, déplacez-vous dans le répertoire `laradock`.

Créez une copie du fichier `env-example` sous le nom de `.env`. Ce dernier est utilisé pour la
construction des images, ainsi que pour passer les variables d'environnement aux conteneurs.

De même, pour configurer le serveur web Nginx, depuis `laradock/`, déplacez-vous dans
`nginx/sites/` et copiez le fichier de configuration `federation-app.conf.example` en
`federation-app.conf`.

Nous pouvons désormais construire les images Docker fournies par Laradock :
[Laradock](http://laradock.io) est un projet qui propose de nombreuses images Docker pour le développement web,
ainsi qu'un fichier Docker-Compose prêt à l'emploi pour orchestrer les conteneurs.
Même si les images ont été adaptées par rapport à ce que propose le projet original,
la construction des images peut être très longue (10 à 30 minutes).
Pour construire les images et créer les conteneurs, déplacez-vous dans le répertoire `laradock/` et lancez :

```sh
docker-compose up -d nginx postgres workspace
```

> Note : Plusieurs volumes sont écrits dans le répertoire local `~/.laradock` (sauf si vous avez décidé de configurer
> le chemin dans le fichier `laradock/.env`). Vous pourriez souhaiter supprimer ces données après utilisation.

Si la création des images et des conteneurs a fonctionné correctement, nous pouvons désormais installer
les dépendances PHP. Depuis le répertoire `laradock`, lancez la commande `bash` dans le conteneur
`workspace` (le conteneur PHP).

```sh
docker-compose exec --user=laradock workspace bash
```

Dans ce terminal, installez les dépendances du projet via Composer :

```sh
composer update
```

Puis, créez la table `utilisateur` utilisée par Symfony pour l'authentification web :

```sh
php bin/console doctrine:migrations:migrate
```

Nous allons désormais créer un premier utilisateur de l'application web.
Toujours dans le conteneur `workspace`, chiffrez le mot de passe utilisateur avec :

```sh
php bin/console security:encode-password
```

Conservez ce mot de passe chiffré, nous l'utiliserons juste après.
Quittez le terminal du conteneur et accédez au client PSQL du conteneur `postgres`.
Si vous n'avez pas modifié la configuration de la base de données,
le login à utiliser est `default` et le mot de passe `secret`.

```sh
docker-compose exec postgres psql --dbname federation --user default
```

Depuis le client, exécutez la requête suivante pour créer un utilisateur `superadmin` et renseignez le mot
de passe chiffré précédemment :

```sql
insert into utilisateur (id, username, roles, password)
values (1, 'superadmin', '["ROLE_SUPER_ADMIN"]', '<MOT DE PASSE CHIFFRÉ>');
```

Vous devriez avoir accès à l'interface à l'adresse <http://localhost>.


### Installation des dépendances sans Docker

Installez PostgreSQL, PHP et le gestionnaire de paquets Composer. Configurez le chemin vers la base de
données dans `web/.env`.

Dans le répertoire `web/`, installez les dépendances du projet via Composer :

```sh
composer update
```

Puis, créez la table `utilisateur` utilisée par Symfony pour l'authentification web :

```sh
php bin/console doctrine:migrations:migrate
```

Nous allons désormais créer un premier utilisateur de l'application web.
Toujours dans le répertoire `web/`, chiffrez le mot de passe utilisateur avec :

```sh
php bin/console security:encode-password
```

Conservez ce mot de passe chiffré, nous l'utiliserons juste après.
Accédez au client PSQL de votre machine.

Depuis le client, exécutez la requête suivante pour créer un utilisateur `superadmin` et renseignez le mot
de passe chiffré précédemment :

```sh
insert into utilisateur (id, username, roles, password)
values (1, 'superadmin', '["ROLE_SUPER_ADMIN"]', '<MOT DE PASSE CHIFFRÉ>');
```

Lancez le serveur de développement avec :

```sh
php bin/console server:start
```

Vous devriez avoir accès à l'interface à l'adresse <http://localhost:8000>.
